<%-- 
    Document   : index
    Created on : Apr 6, 2020, 4:51:02 PM
    Author     : eahumada
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Evaluacion 01 - Calculo Interes Simple</title>
    </head>
    <body>
        <h1>Calculo de Interes Simple</h1>
        <form action="controlador" method="post">
            <label>Monto</label>
            <input type="number" name="monto" required="required"></input>
            <label>Tasa de Interes</label>
            <input type="number" name="tasaDeInteres" required="required"></input>
            <label>Periodos (Años)</label>
            <input type="number" name="periodos" required="required"></input>
            <input type="submit" value="Enviar"/>
        </form>
    </body>
</html>
