<%-- 
    Document   : resultado
    Created on : Apr 6, 2020, 5:26:38 PM
    Author     : eahumada
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultados</title>
    </head>
    <body>
        <%        
            double valorInteres = (double) request.getAttribute("valorInteres");
            double montoTotal = (double) request.getAttribute("montoTotal");
            double monto = (double) request.getAttribute("monto");
            double tasaDeInteres = (double) request.getAttribute("tasaDeInteres");
            double periodos = (double) request.getAttribute("periodos");

            DecimalFormat formato = new DecimalFormat("###,###,###.##");
            DecimalFormat formatoPorcentaje = new DecimalFormat("###,###,###.##%");

        %>        
        <h1>Resultado de Calculo de Interes.</h1>

        <h4>Tasa de Interes :</h4>
        <h5><%=formatoPorcentaje.format(tasaDeInteres/100)%> </h5>
        <div/>

        <h4>Valor Interes:</h4>
        <h5><%=formato.format(valorInteres)%></h5>
        <div/>

        <h4>Monto Inicial :</h4>
        <h5 class="text-primary"><%=formato.format(monto)%></h5>
        <div/>

        <h4>Periodos:</h4>
        <h5 class="text-primary"><%=formato.format(periodos)%> años</h5>
        <div/>

        <h5 class="text-primary">Monto total <br>(Monto + Interes) </h5>
        <h5 class="text-primary"><%=formato.format(montoTotal)%></h5>
        
        <a href="./">Nuevo calculo</a>

    </body>
</html>
